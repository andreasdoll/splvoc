# splvoc
## Split vocabulary and generates playlists

``splvoc`` is intended to extract words (vocabulary) from audio files which contain vocabulary in two languages, may accompanied by example sentences.
An example for such input is the audio download accompanied with Langenscheidt Wortschatz books.

The extracted vocabulary can subsequently be rearranged and exported as playlists, such that one can repeat the vocabulary not always in the same order.


## Examples
### Extracting

Extracting vocabulary is done manually:

    from splvoc.extract import extract

    extract('/home/you/Grundwortschatz_F/chapter_123.mp3', 
            '/home/you/splitted', 
            languages = ['de', 'fr'])

The sound pieces will be played to you and you have to enter if it's a word (for this languages: ``d``, ``f``) or a sentence giving an example of the vocabulary use (``dd``, ``ff``).

### Playlists

Create a shuffled playlist of length 500, using only chapter 4, 5 and 8, and use one second inbetween the translations:

    from splvoc.playlist import create_playlist

    create_playlist('/home/you/splitted', 
                    '/media/my/mp3player', 
                    length = 500,
                    chapters = [4, 5, 8],
                    wait_time = 1)

Create a playlist using only chapter 9, don't shuffle the order and use only french vocabulary (no translations):

    create_playlist('/home/you/splitted', 
                    '/media/my/mp3player', 
                    chapters = [9],
                    shuffle_order = False,
                    language_order = ['fr'])

Create default playlists for all available chapters:

    from splvoc.playlist import create_bootcamps

    create_bootcamps('/home/you/splitted', 
                     '/media/my/mp3player')


Create default playlists for all available chapters (bootcamps), and different playlists for different knowledge levels (consisting of multiple chapters):

    from splvoc.playlist import create_playlist_set

    knowledge = {'level-1': [8, 9, 11, 12],
                 'level-2': [6, 7, 10],
                 'level-3'; [1, 2, 3, 4, 5]}

    create_playlist_set('/home/you/splitted', 
                        '/media/my/mp3player',
                        knowledge)

If you use the above playlist names, ``level-1`` (beginner) has ``wait_time = 2``, the advanced ``level-2`` has ``1`` second, and ``level-3`` (well known) has ``wait_time = 0.5``.
If you use other names, they'll have ``wait_time = 2``.

## License
MIT, see ``LICENSE``

## Sounds
The [vocabulary separation sound](https://freesound.org/people/LittleRainySeasons/sounds/335908/) and the [playlist end sound](https://freesound.org/people/cabled_mess/sounds/389522/) were created by others and released under the Creative Commons license - thanks.
