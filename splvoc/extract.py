import logging
import os
import numpy as np
import subprocess
from tempfile import NamedTemporaryFile
from pydub import AudioSegment
from pydub.silence import split_on_silence
from pydub.utils import get_player_name

logger = logging.getLogger(__name__)

PLAYER = get_player_name()

def extract(
        inPath,
        outPath,
        languages = ['de', 'fr']):

    """
    Extract vocabulary (and may present sentences) from a audio file.


    Parameters
    ----------

    inPath: string
        Path to an audio file (mp3 format).
    outPath: string
        Path to where a dictionary containing the extracted vocabulary is created.
    languages: list of strings
        Languages in order of appearance in the audio file.
        Defaults to ['de', 'fr'].

    Returns
    -------

    Nothing. As side effect, the extracted vocabulary is outputted
    into outPath/audiofileName/.
    """

    outPath = os.path.join(outPath, os.path.splitext(os.path.basename(inPath))[0])
    if not os.path.exists(outPath):
        os.mkdir(outPath)

    silenceLevel = -np.inf

    audio = AudioSegment.from_mp3(inPath)
    print('Splitting... please wait')
    pieces = split_on_silence(audio,
            min_silence_len=20,
            silence_thresh=silenceLevel)

    voc = AudioSegment.empty()
    lastLang = languages[0]
    lang = languages[0]
    kind = None
    lastKind = None
    counter = 1
    for piece in pieces:
        _play(piece)
        lang, kind = _ask_language(languages)
        if lang != lastLang:
            filename = '%s-%s-%s.mp3'%(lastLang, str(counter).zfill(4), lastKind)
            filePath = os.path.join(outPath, filename)
            voc.export(filePath, format="mp3")
            voc = piece
        else:
            voc += piece
        if lang == languages[0] and lastLang == languages[1] and kind == 'word':
            counter += 1
        lastLang = lang
        lastKind = kind
    filename = '%s-%s-%s.mp3'%(lastLang, str(counter).zfill(4), kind)
    filePath = os.path.join(outPath, filename)
    voc.export(filePath, format="mp3")

def _play(seg):
    with NamedTemporaryFile("w+b", suffix=".wav") as f:
        seg.export(f.name, "wav")
        subprocess.call([PLAYER, "-nodisp", "-autoexit", f.name])

def _ask_language(lang):
    l1 = lang[0][0]
    l2 = lang[1][0]
    assert l1 != l2
    l = input('>>> ')
    if l == l1:
        return lang[0], 'word'
    elif l == l2:
        return lang[1], 'word'
    elif l == l1 + l1:
        return lang[0], 'sentence'
    elif l == l2 + l2:
        return lang[1], 'sentence'
    else:
        return _ask_language(lang)
