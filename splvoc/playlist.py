import logging
import os
import inspect
from random import randint
from pydub import AudioSegment
from collections import OrderedDict

import splvoc.hd as hd

logger = logging.getLogger(__name__)

def create_playlist(
        inPath,
        outPath,
        chapters = None,
        length = 250,
        shuffle_order = True,
        language_order = None,
        wait_time = 2
        ):

    """
    Create a playlist from extracted vocabulary.


    Parameters
    ----------

    inPath: string
        Path to where the directories containing extracted vocabulary reside.
    outPath: string
        Path to where to export the playlist.
    chapters: None, or list of integers
        If None, all available chapters are used. Otherwise, use only vocabulary
        from the given chapters.
        Defaults to None.
    length: integer
        How many items the playlist contains.
        Defaults to 250.
    shuffle_order: boolean
        Order if the vocabulary numbers.
        If True, shuffle the order of vocabulary. Otherwise, use consecutive ordering
        (as given by the chapter).
        Defaults to True.
    language_order: None or list of strings
        Order of the languages.
        If None, the playlist will contain the vocabulary with shuffled languages
        (either language may be first).
        If it is of the form e.g. ['fr'], it will only contain french vocabulary.
        If it is of the form e.g. ['fr', 'de'], then french will be always the first language.
        Defaults to None.
    wait_time: float
        Time in seconds between the translations.

    Returns
    -------

    Nothing. As side effect, it outputs the playlist to outPath.
    """

    hd.assert_existence(outPath)
    vocabulary_per_language, languages = _index_vocabulary(inPath, chapters)
    _assert_correct_language_order(languages, language_order)
    separator = _get_separators(wait_time)

    if not os.path.exists(outPath):
        os.mkdir(outPath)

    for N in range(1, length+1):
        voc = _choose_vocabulary(vocabulary_per_language, shuffle_order, N, languages, language_order)
        if len(voc) == 1:
            unit = [voc[0], separator['wait']]
        else:
            unit = [voc[0], separator['wait'], voc[1], separator['ping']]
        mp3 = AudioSegment.empty()
        for u in unit:
            mp3 += u
        mp3.export(_outfile_name(outPath, N), format="mp3")

    separator['end'].export(_outfile_name(outPath, N+1), format="mp3")

def create_bootcamps(
        inPath,
        outPath,
        ):

    """
    Create playlists for each available chapter (with default parameters).

    Parameters
    ----------

    inPath: string
        Path to where the directories containing extracted vocabulary reside.
    outPath: string
        Playlist will be stored in outPath/bootcamp/chapterNr

    Returns
    -------

    Nothing. As side effect, it outputs the playlist to outPath.
    """

    hd.assert_existence(os.path.join(outPath, 'bootcamp'))

    subdirs = hd.subdirs(inPath)

    for chapter in subdirs:
        nr = int(chapter.split('_')[0])
        create_playlist(
                inPath,
                os.path.join(outPath, 'bootcamp', chapter),
                chapters = [nr],
                )

def create_playlist_set(
        inPath,
        outPath,
        knowledge
        ):

    """
    Create bootcamp playlists and shuffled playlists from a dictionary of knowledge levels.


    Parameters
    ----------

    inPath: string
        Path to where the directories containing extracted vocabulary reside.
    outPath: string
        Path to where to export the playlist.
    knowledge: dictionary consisting of keys (string) and values (list of integers)
        Per value (list of chapter numbers), a shuffled playlist with name key (string)
        will be created.

    Returns
    -------

    Nothing. As side effect, it outputs the playlist to outPath.
    """

    create_bootcamps(inPath, outPath)

    _check_knowledge(knowledge, inPath)

    for level, chapters in knowledge.items():
        create_playlist(
            inPath,
            os.path.join(outPath, level),
            chapters = chapters,
            length = 1000,
            wait_time = _wait_time(level)
            )



def _outfile_name(p, c):
    return os.path.join(p, str(c).zfill(4) + '.mp3')

def _choose_vocabulary(vocabulary, shuffle_order, N, languages, languages_order):
    if shuffle_order:
        nr = randint(0, len(vocabulary[languages[0]])-1)
    else:
        nr = N % len(vocabulary[languages[0]])

    if languages_order == None:
        lang1 = languages[randint(0, 1)]
        lang2 = languages[1] if lang1 == languages[0] else languages[0]
    else:
        if len(languages_order) == 1:
            lang1 = languages_order[0]
        else:
            lang1 = languages_order[0]
            lang2 = languages_order[1]

    output = [AudioSegment.from_mp3(vocabulary[lang1][nr])]
    if languages_order == None or len(languages_order) == 2:
        output.append(AudioSegment.from_mp3(vocabulary[lang2][nr]))
    return output

def _index_vocabulary(path, chapters):
    vocabulary = OrderedDict()
    subdirs = hd.subdirs(path)
    languages = _detect_languages_from(os.path.join(path, subdirs[0]))

    for chapter in sorted(subdirs):
        chapterName = os.path.basename(chapter[:-1])
        chapterNr = int(chapterName.split('_')[0])
        if chapters == None or chapterNr in chapters:
            vocabulary[chapterName] = {}
            mp3files = hd.contained_files(os.path.join(path, chapter), 'mp3')
            for lang in languages:
                vocabulary[chapterName][lang] = {}
            for filePath in mp3files:
                filename = os.path.basename(filePath)
                lang = filename.split('-')[0]
                counter = int(filename.split('-')[1])
                kind = filename.split('-')[2][:-4]
                if kind == 'word':
                    vocabulary[chapterName][lang][counter] = filePath

    vocabulary_per_language = {languages[0]: [], languages[1]: []}
    for chapter in vocabulary.values():
        for lang, mp3s in chapter.items():
            for paths in mp3s.values():
                vocabulary_per_language[lang].append(paths)

    return vocabulary_per_language, languages

def _get_separators(wait_time):
    location = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

    separator = {}
    separator['wait'] = AudioSegment.silent(duration=_millisec(wait_time))
    separator['ping'] = AudioSegment.from_mp3(os.path.join(location, 'sound', 'ping.mp3')) - 2
    separator['end'] =  AudioSegment.from_mp3(os.path.join(location, 'sound', 'end.mp3')) - 4
    return separator

def _millisec(n):
    return int(1000*n)

def _assert_correct_language_order(languages, language_order):
    if language_order != None:
        for l in language_order:
            if l not in languages:
                raise ValueError('Language %s not contained in languages %s'%(l, languages))
        if len(language_order) not in [1,2]:
            raise ValueError('Language order must be of length 1 or 2')

def _check_knowledge(knowledge, inPath):
    subdirs = hd.subdirs(inPath)
    subdirNrs = map(lambda c: int(c.split('_')[0]), subdirs)

    chapters_in_knowledge = []
    for level, chapters in knowledge.items():
        chapters_in_knowledge.extend(chapters)

    missing_chapters = set(subdirNrs) - set(chapters_in_knowledge)
    if len(missing_chapters) > 0:
        print('Chapters available, but not in knowledge: %s'%missing_chapters)

def _wait_time(level):
    if level == 'level-1':
        return 2
    elif level == 'level-2':
        return 1
    elif level == 'level-3':
        return 0.5
    else:
        return 2

def _detect_languages_from(path):
    mp3s = hd.contained_files(path, 'mp3')
    languages = map(lambda f: os.path.basename(f).split('-')[0], mp3s)
    return list(set(languages))
