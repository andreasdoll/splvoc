import os
from glob import glob

def subdirs(path):
    dirs = [s for s in os.listdir(path) if os.path.isdir(os.path.join(path, s))]
    return sorted(dirs)

def contained_files(path, ext):
    return sorted(glob(os.path.join(path, '*.' + ext)))

def assert_existence(path):
    if not os.path.exists(path):
        os.mkdir(path)
