from setuptools import setup, find_packages

setup(
    name = 'splvoc',
    description = 'Extract words (vocabulary) from mp3 files and rearrange them as (randomized) playlists.',
    version = '1.0',
    author = 'Andreas Doll',
    author_email = 'fuseki@posteo.de',
    url = 'http://bitbucket.org/andreasdoll/splvoc',
    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Topic :: Education',
        'Topic :: Communications',
        ],
    keywords = 'Wortschatz, Langenscheidt, vocabulary, playlist, language, learn, learning',
    license = 'MIT',
    install_requires = [
        'setuptools',
        'pydub',
        'numpy'
        ],
    packages = find_packages(),
    )
