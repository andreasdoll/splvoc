from splvoc.extract import extract
import splvoc.playlist as playlist 

knowledge = {'level-1': [7, 14, 78, 79, 77, 52, 51, 50],
             'level-2': [15, 8, 4, 5, 9, 10, 11, 12, 13, 74, 75, 82, 83, 85, 86, 87, 88, 89, 90, 91],
             'level-3': [1, 2, 3, 6, 71, 72, 73, 80, 81, 84, 92]}

extract('/home/you/Grundwortschatz/original',
        '/home/you/Grundwortschatz/splitted')

playlist.create_playlist(
        '/home/you/Grundwortschatz/splitted',
        '/media/your/mp3player',
        chapters = knowledge['level-1'],
        wait_time = 2,
        length = 1000)

playlist.create_playlist_set(
        '/home/you/Grundwortschatz/splitted',
        '/media/your/mp3player',
        knowledge)
